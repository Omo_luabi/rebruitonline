<?php

class JobsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $user_details = new Zend_Session_Namespace('user_details');
        $this->submitAction();
        if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
            $this->userid = $this->view->user_id = $user_details->user_id;
            $this->userid = $user_details->user_id;
            $this->view->user_name = $user_details->user_name;
            $this->view->user_full_name = $user_details->user_full_name;
            $this->view->user_image = $user_details->user_image;
            $this->user_sector = $user_details->user_sector ;
        }else{
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }

    public function indexAction()
    {
        /*$db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join("users", "job_post_company_id =user_id", array("user_name"))
                ->limit(20);
        $jobs_array = $db_jobs->fetchAll($select_jobs_object);
        
        var_dump($jobs_array->toArray());*/
        $this->view->all = $this->getAllJobs();
        $this->view->matched = $this->getMatchedJobs();
        $this->view->applied = $this->getAppliedJobs();
//        $this->view->saved = getSavedJobs();
//        $this->view->referred = getRefferedJobs();
    }
    
    function getAllJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join("users", "job_post_company_id =user_id", array("user_name"))
                ->order("job_post_id desc")
                ->limit(20);
        $jobs_array = $db_jobs->fetchAll($select_jobs_object);
        return $jobs_array->toArray();
    }
    
    function getMatchedJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join("users", "job_post_company_id =user_id", array("user_name"))
                ->where("job_post_sector = $this->user_sector")
                ->limit(20);
//        echo $select_jobs_object__toString();
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object)->toArray();
    }

    function getAppliedJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->joinLeft('rebruit_job_applications', ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object)->toArray();
    }

    function getSavedJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->joinLeft('rebruit_job_applications', ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }

    function getRefferedJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->joinLeft('rebruit_job_applications', ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }

}

