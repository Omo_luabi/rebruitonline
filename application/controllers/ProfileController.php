<?php

class ProfileController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $user_details = new Zend_Session_Namespace('user_details');
        $this->submitAction();
        if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
            $this->userid = $this->view->user_id = $user_details->user_id;
            $this->view->user_name = $user_details->user_name;
            $this->view->user_full_name = $user_details->user_full_name;
            $this->view->user_image = $user_details->user_image;
        }else{
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }

    public function indexAction()
    {
        // action body
        $user = new Application_Model_DbTable_Users();
        $user_ = $user->fetchRow($where="user_id='$this->userid'");
        //echo "USR VAL for ".$this->userid;
        //var_dump($user_ );
        $this->view->user_id = $user_->user_id;
        $this->view->user_name = $user_->user_name;
        $this->view->full_name = $user_->user_last_name . " ". $user_->user_first_name . " ".$user_->user_middle_name;
        
        $this->view->firstname = $user_->user_first_name;
        $this->view->middlename = $user_->user_middle_name;
        $this->view->lastname = $user_->user_last_name;

        $this->view->dob = $user_->user_dob;
        $this->view->gender = $user_->user_gender;
        $this->view->marital = $user_->user_marital_status;
        $this->view->country = $user_->user_country_id;
        $this->view->user_email = $user_->user_email;
        $this->view->user_type = $user_->user_type;
        $this->view->user_phone = $user_->user_phone;
        $this->view->address = $user_->user_address;
        $this->view->user_sector = $user_->user_sector;
        $this->view->user_employment_status = $user_->user_employment_status;
        $this->view->user_job_preference = $user_->user_job_preference;
        
        $db_awards_tb = new Zend_Db_Table('rebruit_awards');
        $this->view->awards = $db_awards_tb->fetchAll($where="rebruit_user_id='$this->userid'")->toArray();
        /*$db_awards = $db_awards_tb->select()->setIntegrityCheck(false);
        $db_awards->from('rebruit_awards')
                ->where("rebruit_user_id='$this->userid'") ;
//        var_dump($db_awards_tb->fetchAll($db_awards)->toArray());
//        echo "</br>" . $this->userid;
        $this->view->awards = $db_awards_tb->fetchAll($db_awards_tb->fetchAll($db_awards)->toArray());*/
        
        $db_cert = new Zend_Db_Table('rebruit_certifications');
        $this->view->cert = $db_cert->fetchAll($where="rebruit_user_id='$this->userid'")->toArray();
        //var_dump($this->view->cert);
        
        $db_edu = new Zend_Db_Table('rebruit_education');
        $this->view->edu = $db_edu->fetchAll($where="rebruit_user_id='$this->userid'")->toArray();
        //var_dump($this->view->edu);
        $db_exp = new Zend_Db_Table('rebruit_experience');
        $this->view->exp = $db_exp->fetchAll($where="rebruit_user_id='$this->userid'")->toArray();
        
        $db_skills = new Zend_Db_Table('rebruit_skills');
        $this->view->skills = $db_skills->fetchAll($where="rebruit_user_id='$this->userid'")->toArray();
        
        $db_sect = new Zend_Db_Table('rebruit_sectors');
        $this->view->sectors = $db_sect->fetchAll()->toArray();
    }
    
    public function detailsAction()
    {
        // action body
    }

}

