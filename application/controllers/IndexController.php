<?php

class IndexController extends Zend_Controller_Action
{
    //Included try catch in the init function below
    //enable mod_rewrite in apache
    public function init()
    {
        /* Initialize action controller here */
        try{
            $user_details = new Zend_Session_Namespace('user_details');
            $this->submitAction();
            if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
                $this->view->user_id = $user_details->user_id;
                $this->view->user_name = $user_details->user_name;
                $this->view->user_full_name = $user_details->user_full_name;
                $this->view->user_image = $user_details->user_image;
                $this->view->user_type = $user_details->user_type;
            }else{
                $urlOptions = array('controller'=>'Auth', 'action'=>'index');
                $this->_helper->redirector->gotoRoute($urlOptions);
            }
        }Catch(Exception $er){
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
    }

    public function indexAction()
    {
        $user_id = 1;
        $this->view->connections = [];
        $db_sect = new Zend_Db_Table('rebruit_sectors');
        $this->view->sectors = $db_sect->fetchAll()->toArray();
        /*$db     = new Zend_Db_Table('posts');
        $select_object = $db->select()->setIntegrityCheck(false);
        $select_object->from('posts')
                ->join('users', 'users.user_id = posts.user_id', array('user_name'))
                ->where("message_type = ?", 'job')
                ->orWhere('message_to = ?', $user_id)
                ->orWhere('posts.user_id = ?', $user_id);
       
        $post_read_array = $db->fetchAll($select_object)->toArray();
        print_r($post_read_array);
        
        $userdb     = new Zend_Db_Table('users');
        $my_profile = $userdb->fetchRow($where="user_id='$user_id'")->toArray();
        echo "My profile name is ". $my_profile['user_name'] . " <br/> and it has a length of ".strlen($my_profile['user_name'])."</br>";
        var_dump($my_profile);
                  */
        
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }


}

