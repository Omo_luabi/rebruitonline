<?php

class MessagesController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $user_details = new Zend_Session_Namespace('user_details');
        $this->submitAction();
        if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
            $this->userid = $this->view->user_id = $user_details->user_id;
            $this->view->user_name = $user_details->user_name;
            $this->view->user_full_name = $user_details->user_full_name;
            $this->view->user_image = $user_details->user_image;
        }else{
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }

    public function indexAction()
    {
//        $contacts = new Zend_Db_Table('users');
//        $contacts_object = $contacts->select()->setIntegrityCheck(false);
//        $contacts_object->from('users', array("user_name","user_id","user_first_name","user_last_name","user_middle_name"))
//                ->where("user_type = 3")
//                ->limit(20);
        
//        $contacts_array = $contacts->fetchAll($contacts_object);
//        $this->view->contacts = $contacts_array->toArray();
        $spec = "rebruit_posts.post_to_id";
        $spec2 = "rebruit_posts.post_user_id";
        $db     = new Zend_Db_Table('rebruit_posts');
        $select_object = $db->select()->setIntegrityCheck(false); 
        $select_object->from('rebruit_posts')
            ->join('users', 'users.user_id = rebruit_posts.post_user_id', array('user_name','user_image AS userimage','user_id','user_first_name','user_last_name'))
            ->join(array('r'=>'users'), 'r.user_id = rebruit_posts.post_to_id', array('user_name AS receiver_name', 'user_id AS receiver_id'))
            ->where("rebruit_posts.post_type = 'inbox'")
            ->where('rebruit_posts.post_to_id = ?', $this->userid)
            ->orWhere('rebruit_posts.post_user_id = ?', $this->userid)
            ->order("rebruit_posts.post_id desc")
                ->group($spec)
            ->group($spec2);
        //echo $select_object->__toString();
        $array_list = $db->fetchAll($select_object);
        $this->view->contacts = $array_list->toArray();
        // $this->view->contacts = $contacts_object->__toString();
        //         return;
    }
}

