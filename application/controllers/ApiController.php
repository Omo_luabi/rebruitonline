<?php

class ApiController extends Zend_Controller_Action
{
//6234221933
    public function init()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout('authlayout');
        $this->_todo = array (
          "1" => "Rebruit 1",
          "2" => "Rebruit 2",
          "3" => "Rebruit 3"
        );
    }
    
    function testConn(){
        $dsn = 'mysql:dbname=rebruit;host=127.0.0.1';
        $user = 'rumolar1_rebruit';
        $password = '2014Rebruit2.0';

        try {
            $dbh = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function indexAction()
    {
        /*
         * copied the sock file to make mysql work on mac, but created the folder inside var first
         * sudo ln -s /tmp/mysql.sock /var/mysql/mysql.sock
         * 
         */
        //phpinfo();
//        var_dump($this->authenticate("info@rebruit.com", "73852"));
        //var_dump($this->testConn());
        //return;
//        // the message
//        $msg = "First line of text\nSecond line of text \n";
//        echo $msg;
//        // use wordwrap() if lines are longer than 70 characters
//        $msg = wordwrap($msg,70);
//
//        // send email
//        mail("alli5723@gmail.com","My subject",$msg);
        $local = str_replace("localhost", "localhost/rebruitonline/public","http://".$_SERVER['HTTP_HOST'] );
        $newurl = $local . "/template.php";    
        $register_temp = file_get_contents($newurl);
            echo $newurl;
            echo $register_temp;
//        try{
//            $user_details = new Zend_Session_Namespace('user_details');
//            $user_id = 0;
//        }catch(Exception $er){
//
//        }
//        
//        if(isset($user_details->user_id)){
//            $this->userid = $user_id = $user_details->user_id;
//            $this->user_sector = $user_details->user_sector;
//        }
//        var_dump($this->read_post(1));
        //var_dump($this->like_bruit_comment("1","6", "like"));
        echo "Please specify an action";
        //echo Zend_Json::encode($this->_todo);
    }

    public function registerAction()
    {
        // action body
    }

    public function getAction()
    {
        // action body
        echo "No get";
    }
    
    function smtpSend($subject, $msg, $recipient_array){
        // Create transport
        $transport = new Zend_Mail_Transport_Smtp();

        $protocol = new Zend_Mail_Protocol_Smtp('mail.rebruit.com');
        $protocol->connect();
        $protocol->helo();

        $transport->setConnection($protocol);

        // Loop through messages
        for ($i = 0; $i < count($recipient_array); $i++) {
            $mail = new Zend_Mail();
            $mail->addTo($recipient_array[$i], '');
            $mail->setFrom('noreply@rebruit.com', 'Rebruit team');
            $mail->setSubject(
                $subject
            );
            $mail->setBodyText('');
            $mail->setBodyHtml($msg);

            // Manually control the connection
            $protocol->rset();
            $mail->send($transport);
        }

        $protocol->quit();
        $protocol->disconnect();
    }
    
    function plainSend(){
        $tr = new Zend_Mail_Transport_Sendmail('-freturn_to_me@rebruit.com');
        Zend_Mail::setDefaultTransport($tr);

        $mail = new Zend_Mail();
        $mail->setBodyText('This is the text of the mail using plain send method.');
        $mail->setBodyHtml('My Nice <b>Test</b> Text');
        $mail->setFrom('info@rebruit.com', 'Some Sender');
        $mail->addTo('alli5723@gmail.com', 'Some Recipient');
        $mail->setSubject('TestSubject');
        $mail->send();
    }

    public function postAction()
    {
        $action = $this->_request->getPost('action');
        //$this->_todo[count($this->_todo) + 1] = $item; $_POST['action']
        try{
            $user_details = new Zend_Session_Namespace('user_details');
            $user_id = 0;
        }catch(Exception $er){

        }
        
        if(isset($user_details->user_id)){
            $this->userid = $user_id = $user_details->user_id;
            $this->user_sector = $user_details->user_sector;
        }
        $result = '';
        switch ($action) {
            case 'auth':
                $result = $this->authenticate($this->_request->getPost('usr'), $this->_request->getPost('pw'));
            break;
            case 'register':
                $result = $this->register($this->_request->getPost('email'), $this->_request->getPost('mobile'), $this->_request->getPost('pw'));
            break;
            case 'submit_post':
                $result = $this->submit_post($this->_request->getPost('post_user_id'),$this->_request->getPost('post_content'),$this->_request->getPost('post_type'));
            break;
            case 'submit_comment' :
                $result = $this->submit_comment($user_id);
                break;
            case 'read_post':
                $result = $this->read_post($this->_request->getPost('user_id'));
            break;
            case 'read_message':
                $result = $this->read_message($this->userid, $this->_request->getPost('id'));
            break;
            case 'submit_job_post':
                $result = $this->submit_job_post($user_id);
            break;
            case 'like_bruit_comment':
                $result = $this->like_bruit_comment($this->_request->getPost('user_id'),$this->_request->getPost('post_id'),$this->_request->getPost('type'));
            break;
            case 'job_detail':
                $result = $this->job_detail($user_id,$this->_request->getPost('post_id'));
            break;
            case 'update_personal_profile':
                $result = $this->update_personal_profile($user_id);
            break;
            case 'update_other_profile':
                $result = $this->update_other_profile($user_id);
            break;
            case 'update_profile_picture':
                $result = $this->update_profile_picture($user_id);
            break;
            case 'save_apply_jobs':
                $result = $this->save_apply_jobs($user_id);
            break;
            case 'forgot_password':
                $result = $this->forgot_password();
            break;
            case 'fetch_country':
                $result = $this->fetch_country();
            break;
            case 'search_string':
                $this->search_string($this->_request->getPost('keyword'));
                return;
            break;
            default:
                $result = "No Action Specified";
            break;
        }
        //echo Zend_Json::encode($this->_todo);
        echo Zend_Json::encode($result);
    }
    
    function search_string($keyword){
        extract($_POST);
        $userid = $this->userid;
        $users = new Zend_Db_Table('users');
        //select_item(\''.str_replace("'", "\'", $rs).'\')
        $return_string = '<li onclick="select_item(\'[ID]\',\'[NAME]\')"><div class="search-group"><div class="search-title">[NAME]</div><div class="search-under-title">[OPTIONS]</div></div></div></li>';
       
        $search_query = $users->fetchAll($where="user_name LIKE '%$keyword%' OR user_first_name  LIKE '%$keyword%' OR user_last_name  LIKE '%$keyword%' OR user_email  LIKE '%$keyword%'", $order="user_name");
        $search_query_array = $search_query->toArray();
//        var_dump($search_query_array);
        for($i = 0; $i < count($search_query_array); $i++){
            //$return_string = '<li onclick="set_item(\''.str_replace("'", "\'", $rs).'\')"><div class="search-group"><div class="search-title">[NAME]</div><div class="search-under-title">[OPTIONS]</div></div></div></li>';
            $return_string = '<li style="cursor:pointer;" onclick="select_item(\'[ID]\',\'[NAME]\')"><div class="search-group"><div class="search-title">[NAME]</div><div class="search-under-title">[OPTIONS]</div></div></div></li>';
            if(isset($type)){
                $return_string = '<li style="cursor:pointer;" onclick="select_item2(\'[ID]\',\'[NAME]\')"><div class="search-group"><div class="search-title">[NAME]</div><div class="search-under-title">[OPTIONS]</div></div></div></li>';
            }
            $id = $search_query_array[$i]['user_id'];
            $name = $search_query_array[$i]['user_name'];
//          echo $name;
            if($search_query_array[$i]['user_first_name'] != ""){
                $name = $search_query_array[$i]['user_first_name'] . " ";
            }
            if($search_query_array[$i]['user_last_name'] != ""){
                $name .= $search_query_array[$i]['user_last_name'];
            }
            $return_string = str_replace("[ID]", $id, $return_string);
            $return_string = str_replace("[NAME]", $name, $return_string);
            $return_string = str_replace("[OPTIONS]", $keyword, $return_string);
            echo $return_string;
        }
        
//        echo $return_string; i will have to append if i wanna use this, not sure which is a better option
                                  
    }

    public function putAction()
    {
        // action body
    }

    public function deleteAction()
    {
        // action body
    }
    
    public function fetch_country(){
        $country = new Zend_Db_Table('rebruit_country');
        $country_list = $country->fetchAll()->toArray();
        return array("data"=>array("status"=>"1", "status_detail"=>"Country Listing.", "result"=>$country_list));
    }
    
    function save_apply_jobs($user_id){
        $jobapply = new Zend_Db_Table('rebruit_job_applications');
        extract($_POST);
        $typecolumn = $action_type."_job";
        $save_applied = $jobapply->fetchRow($where="rebruit_user_id='$user_id' AND job_post_id ='$job_post_id'");
        //return $_POST;
        
        if($save_applied != null && $save_applied != 'null'){
            if(count($save_applied) > 0 && $save_applied[$typecolumn] === 1){
                //return $typecolumn;//$save_applied[$typecolumn];
                return array("data"=>array("status"=>"0", "status_detail"=>"You have performed this operation previously."));
            }else if(count($save_applied) > 0 && $save_applied[$typecolumn] != 1){
                $appid = $save_applied['application_id'];
                $upd = array($typecolumn=>1);
                $id = $jobapply->update($upd, "application_id='$appid'");
                if($id){
                    $user_details = new Zend_Session_Namespace('user_details');
//                    $user_details->user_image = $img;
                    return array("data"=>array("status"=>"1", "status_detail"=>"Action was successful.", "result"=>$id));
                }else{
                    return array("data"=>array("status"=>"0", "status_detail"=>"Error performing selection operation."));
                }
            }
        }else{
            //return $typecolumn;
            //return "done";
            $post_array = array("job_post_id"=>$job_post_id, "rebruit_user_id"=>$user_id,$typecolumn=>"1");
            $id = $jobapply->insert($post_array);
            if($id){
                return array("data"=>array("status"=>"1", "status_detail"=>"Action performed was successful.", "result"=>$id));
            }else{
                return array("data"=>array("status"=>"0", "status_detail"=>"Error performing selection operation."));
            }
        }
        
    }

    function update_profile_picture($user_id){
        $user = new Zend_Db_Table('users');
        //"uploads/thumb_1geekmode-6532075651.jpg";//
        $img = $this->_request->getPost('user_image');
        $upd = array("user_image"=>$img);

        $id = $user->update($upd, "user_id='$user_id'");
        if($id){
            $user_details = new Zend_Session_Namespace('user_details');
                $user_details->user_image = $img;
            return array("data"=>array("status"=>"1", "status_detail"=>"Successfully Updated Personal Profile.", "result"=>$id));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error Saving the uploaded file."));
        }
    }
    
    function update_other_profile($user_id){
        $user = new Application_Model_DbTable_Users();
        extract($_POST);
        $db_name = '';
        $post_array = '';
        $dt = date("Y-m-d H:i:s");
        //insert into respective database based on the 
        switch ($div){
            case 'education_profile':
                $db_name = 'rebruit_education';
            break;
            case 'certification_profile':
                $db_name = 'rebruit_certifications';
            break;
            case 'experience_profile':
                $db_name = 'rebruit_experience';
            break;
            case 'skill_profile':
                $db_name = 'rebruit_skills';
            break;
            case 'award_profile':
                $db_name = 'rebruit_awards';
            break;
        }
        $posts     = new Zend_Db_Table($db_name);
        $changeIds = '';
        $array_extract = explode(",", $array);
        //return array("1"=>$array,"2"=>$array_extract);
        for($i = 0; $i < count($array_extract); $i++){
            
            switch ($div){
                case 'education_profile':
                    $edu_grade = 'edu_grade_'.$array_extract[$i];
                    $post_array = array(
                        "rebruit_user_id"=> $user_id, 
                        "education_grade"=> $$edu_grade, 
                        "education_certification"=>${'edu_qualification_'.$array_extract[$i]}, 
                                "education_institution"=>${'edu_institution_'.$array_extract[$i]}, 
//                                        "education_year_from"=>${'edu_yearin_'.$array_extract[$i]}, 
                                        "education_year_from"=>${''}, 
                                                "education_year_to"=>${'edu_yearout_'.$array_extract[$i]}, 
                                                        "education_modified_date"=>$dt);
                break;
                case 'certification_profile':
                    $post_array = array(
                        "rebruit_user_id"=> $user_id, 
                        "certification_provider"=> ${'cert_provider_'.$array_extract[$i]}, 
                        "certification_certification"=>${'cert_certification_'.$array_extract[$i]}, 
                                "certification_year"=>${'cert_year_'.$array_extract[$i]}, 
//                                        "certification_remark"=>${'cert_remark_'.$array_extract[$i]});
                                        "certification_remark"=>${''});
                break;
                case 'experience_profile':
                    $post_array = array(
                        "rebruit_user_id"=> $user_id, 
                        "work_company"=> ${'exp_company_'.$array_extract[$i]}, 
                        "work_job_title"=>${'exp_title_'.$array_extract[$i]}, 
                            "work_year_from"=>${'exp_from_'.$array_extract[$i]}, 
                                "work_job_description"=>${'exp_desc_'.$array_extract[$i]}, 
                                    "work_year_to"=>${'exp_to_'.$array_extract[$i]});
                break;
                case 'skill_profile':
                    $post_array = array(
                        "rebruit_user_id"=> $user_id, 
                        "skill_type"=> ${''}, 
                        "skill_detail"=>${'skill_detail_'.$array_extract[$i]}, 
                                "year_acquired"=>${''});
                break;
                case 'award_profile':
                    $post_array = array(
                        "rebruit_user_id"=> $user_id, 
                        "award_type"=> ${'award_type_'.$array_extract[$i]}, 
                        "award_body"=>${'award_body_'.$array_extract[$i]}, 
                                "year_acquired"=>${'award_year_'.$array_extract[$i]});
                break;
            }
            //return $post_array;
           $id = $posts->insert($post_array);
            if($id){
                $changeIds .= $id . ",";
            } 
        }
        
        if($changeIds){
            return array("data"=>array("status"=>"1", "status_detail"=>"Successfully Inserted the profile update.", "result"=>$changeIds));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error updating profile.", "result"=>$id));
        }
        
    }
    
    public function update_personal_profile($user_id){
        $user = new Application_Model_DbTable_Users();
        
        // $fullname = explode(" ", $this->_request->getPost('full_name'));
        // $dob = $this->_request->getPost('dob');
        // $gender = $this->_request->getPost('gender');
        // $marital = $this->_request->getPost('marital');
        // $country = $this->_request->getPost('country');
        // $address = $this->_request->getPost('address');
        // $upd = array("user_last_name"=>$fullname[0],
        //         "user_first_name"=>$fullname[1],
        //         "user_dob"=>$dob,
        //         "user_gender"=>$gender,
        //         "user_marital_status"=>$marital,
        //         "user_country_id"=>$country,
        //         "user_address"=>$address
        //     );
        extract($_POST);
        $user_details = new Zend_Session_Namespace('user_details');
        $user_details->user_firstname = $p_firstname;
        $user_details->user_middlename = $p_middlename;
        $user_details->user_lastname = $p_surname;
        $user_details->user_full_name =  $p_firstname . " ".$p_middlename . " ". $p_surname;
        
        $upd = array(
                "user_last_name"=>$p_surname,
                "user_middle_name"=>$p_middlename,
                "user_first_name"=>$p_firstname,
                "user_dob"=>$p_dob,
                "user_gender"=>$p_gender,
                "user_sector"=>$p_sector,
                "user_job_preference"=>$p_job_pref,
                "user_marital_status"=>$p_marital,
                "user_email"=>$p_email,
                "user_phone"=>$p_mob_num,
                "user_employment_status"=>$p_emp_status,
                "user_address"=>$p_address
            );
        if(trim($p_nationality) > 0){
            $upd['user_country_id'] = $p_nationality;
        }

        $id = $user->update($upd, "user_id='$user_id'");
        if($id){
            $user_details->user_sector = $p_sector;
            return array("data"=>array("status"=>"1", "status_detail"=>"Successfully Updated Personal Profile.", "result"=>$id));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error Updating."));
        }
    }
    
    public function like_bruit_comment($user_id,$post_id, $type){
        //$posts = new Application_Model_DbTable_Posts();
        if($type === "delete"){
            $db = Zend_Db_Table::getDefaultAdapter();
            $db->delete('rebruit_posts', array(
                'post_id = ?' => $post_id,
                'post_user_id = ?' => $user_id
            ));
            return array("data"=>array("status"=>"1", "status_detail"=>"Successful.", "result"=>$id));
        }
        $posts     = new Zend_Db_Table('rebruit_posts');
        $tp = "post_".$type;
        //$upd = array($tp=>$user_id.",");
        $newId = $user_id.",";
//        $upd = array($tp => "CONCAT(COALESCE($tp,''), '$newId')");
        $upd = array($tp => new Zend_Db_Expr("CONCAT($tp, '$newId')"));
        //return $upd;
        //Update rebruit_posts set post_like = CONCAT(post_like,'2,') where post_id = 7 AND post_like NOT LIKE '%2,%'
        $id = $posts->update($upd, "post_id=$post_id AND $tp NOT LIKE '%$newId%'");
        
        if($id){
            return array("data"=>array("status"=>"1", "status_detail"=>"Successful.", "result"=>$id));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"You have already liked this post.", "result"=>$id));
        }
        
    }

    function job_detail($user_id,$post_id){
        
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join("users", "job_post_company_id =user_id", array("user_name"))
                ->where("job_post_id = $post_id")
                ->limit(20);
                //return $select_jobs_object->__toString();
        $jobs_array = $db_jobs->fetchAll($select_jobs_object);
        //return $jobs_array;
        if($jobs_array){
            $j = $jobs_array[0]['job_post_description'];
            $t = $jobs_array[0]['job_post_title'];
            return array("data"=>array("status"=>"1", "status_detail"=>"Successful.", "result"=>$j, "title"=>$t));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"We couldn't fetch this Job details", "result"=>""));
        }
    }
    
    function submit_comment($user_id){
        $comments = new Zend_Db_Table('rebruit_comments');
        extract($_POST);
        $comment_array = array("comment_post_id"=>$post_id,
            "comment_user_id"=>$user_id,
            "comment_content"=>$comment);
        $id = $comments->insert($comment_array);
        if($id){
            return array("data"=>array("status"=>"1", "status_detail"=>"Comment added successfully.", "result"=>$id));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error adding comment.", "result"=>""));
        }
    }
    
    function submit_job_post($user_id){
        //submits this job to the job table 
        //inserts it with reference into posts table
        /*
         * post_content:title,
                post_desc:desc,
                post_end_date:end_date,
                post_sector:sector,
                post_salary:salary,
         */
        $posts     = new Zend_Db_Table('rebruit_jobs');
        $dt = date("Y-m-d H:i:s");
        extract($_POST);
        //$this->user_sector
        $post_array = array("job_post_sector"=>$post_sector, 
            "job_post_company_id"=>$post_user_id, 
            "job_post_title"=>$post_content, 
            "job_post_description"=>$post_desc,
            "job_post_date"=>$dt,
            "job_post_due"=>$post_end_date,
            "job_post_salary"=>$post_salary,
            "job_vacancy_company"=>$company,
            "job_vacancy_location"=>$location);
        //add salary, change due date to varchar datatype
        $id = $posts->insert($post_array);
        if($id){
            $response = $this->submit_post($post_user_id, $post_content, $post_type, $id);
            return $response;
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error submitting Job post.", "result"=>$id));
        }
    }
    
    function submit_post($user_id,$message_content, $message_type, $post_ref = 0){
        //$posts = new Application_Model_DbTable_Posts();
        if($message_type === "inbox2"){
            $message_type = "inbox";
        }
        $posts     = new Zend_Db_Table('rebruit_posts');
        extract($_POST);
        if($post_ref > 0){
            $post_array = array("post_user_id"=>$user_id, "post_content"=>$message_content, "post_type"=>$message_type, "post_reference"=>$post_ref , "post_like"=>"0,", "post_bruit"=>"0,", "post_comment"=>"0,");
        }else{
            $post_array = array("post_user_id"=>$user_id, "post_content"=>$message_content, "post_type"=>$message_type, "post_to_id"=>$post_to_id, "post_like"=>"0,", "post_bruit"=>"0,", "post_comment"=>"0,");
        }
        
        $dt = date("Y-m-d H:i:s");
        $id = $posts->insert($post_array);
        if($id){
            return array("data"=>array("status"=>"1", "status_detail"=>"Successfully Submitted.", "result"=>$id, "time"=>$dt));
        }else{
            return array("data"=>array("status"=>"0", "status_detail"=>"Error Submitting.", "result"=>$id));
        }
    }

    function getMatchedJobs(){
        /**Applied JOBS**/
        //SELECT * FROM rebruit_jobs j LEFT JOIN (SELECT * FROM job_applications where job_applicant_id = 1) a on j.job_post_id = a.job

        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join("users", "job_post_company_id =user_id", array("user_name"))
                ->where("job_post_sector = $this->user_sector")
                ->limit(20);
                //return $select_jobs_object->__toString();
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);

    }

    function getAppliedJobs(){
        /**Applied JOBS**/
        //SELECT * FROM rebruit_jobs j LEFT JOIN (SELECT * FROM job_applications where job_applicant_id = 1) a on j.job_post_id = a.job
        //SELECT * FROM rebruit_jobs j JOIN (SELECT * FROM rebruit_job_applications where rebruit_user_id = 1) a on j.job_post_id = a.job_post_id
        /*$db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->joinLeft('rebruit_job_applications', ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);*/
        $db_sub_app = new Zend_Db_Table('rebruit_job_applications');
        $db_sub_app_ = $db_sub_app->select()
                        ->from('rebruit_job_applications')
                        ->where("rebruit_user_id = $this->userid AND apply_job=1");

        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join(array('rebruit_job_applications' => $db_sub_app_), ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        //return $select_jobs_object->__toString();
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }

    function getSavedJobs(){
        /**Applied JOBS**/
        //SELECT * FROM rebruit_jobs j LEFT JOIN (SELECT * FROM job_applications where job_applicant_id = 1) a on j.job_post_id = a.job

        $db_sub_app = new Zend_Db_Table('rebruit_job_applications');
        $db_sub_app_ = $db_sub_app->select()
                        ->from('rebruit_job_applications')
                        ->where("rebruit_user_id = $this->userid AND save_job=1");

        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->join(array('rebruit_job_applications' => $db_sub_app_), ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        //return $select_jobs_object->__toString();
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }

    function getRefferedJobs(){
        /**Applied JOBS**/
        //SELECT * FROM rebruit_jobs j LEFT JOIN (SELECT * FROM job_applications where job_applicant_id = 1) a on j.job_post_id = a.job

        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs')
                ->joinLeft('rebruit_job_applications', ' rebruit_jobs.job_post_id = rebruit_job_applications.job_post_id');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }

    function getRecentJobs(){
        $db_jobs = new Zend_Db_Table('rebruit_jobs');
        $select_jobs_object = $db_jobs->select()->setIntegrityCheck(false);
        $select_jobs_object->from('rebruit_jobs');
        return $jobs_array = $db_jobs->fetchAll($select_jobs_object);
    }
    
    function read_message($userid, $userid2){
        $db     = new Zend_Db_Table('rebruit_posts');
        $select_object = $db->select()->setIntegrityCheck(false); 
        $select_object->from('rebruit_posts')
                ->where("rebruit_posts.post_type = 'inbox'")
                ->where('rebruit_posts.post_to_id = ?', $userid2)
                ->orWhere('rebruit_posts.post_user_id = ?', $userid2)
                ->order("rebruit_posts.post_id desc");
        $post_read_array = $db->fetchAll($select_object);
        return array("data"=>array("status"=>"1", "status_detail"=>"Success", "result"=>$post_read_array->toArray()));
    }
    
    function read_post($user_id){
        $db     = new Zend_Db_Table('rebruit_posts');
        $select_object = $db->select()->setIntegrityCheck(false); 
        $select_object->from('rebruit_posts')
                ->join('users', 'users.user_id = rebruit_posts.post_user_id', array('user_name','user_image','user_first_name','user_last_name','user_type'))
                
                ->joinLeft('rebruit_jobs', 'rebruit_jobs.job_post_id = rebruit_posts.post_reference', array('job_post_description','job_post_due','job_post_salary','job_vacancy_company','job_vacancy_location'))
                ->where("rebruit_posts.post_type = 'job' AND rebruit_posts.post_privacy='public'")
                ->orWhere('rebruit_posts.post_to_id = ?', $user_id)
                ->orWhere('rebruit_posts.post_user_id = ?', $user_id)
                ->order("rebruit_posts.post_id desc");
        
        $post_read_array = $db->fetchAll($select_object);
        
        $matchedJobs = $this->getMatchedJobs(); 
        $appliedJobs = $this->getAppliedJobs();
        $savedJobs = $this->getSavedJobs();
        $refferedJobs = $this->getRecentJobs();

        
        //$posts = new Application_Model_DbTable_Posts();
        //$post_read_array = $posts->fetchAll($where="user_id='$user_id' OR message_to='$user_id' OR (message_type='job' AND post_privacy='public')");
        $userdb     = new Zend_Db_Table('users');
        $my_profile = $userdb->fetchRow($where="user_id='$user_id'")->toArray();
        $pass_mark = 6;
        if(strlen($my_profile['user_first_name']) < 1 || strlen($my_profile['user_last_name']) < 1){
            $pass_mark -= 1;
        }
        if(strlen($my_profile['user_address']) < 1){ $pass_mark -= 1; }
        
        if(strlen($my_profile['user_gender']) < 1){ $pass_mark -= 3; }//Marital status, gender and dob
        
        if(strlen($my_profile['user_country_id']) < 1){ $pass_mark -= 1; }
        
        if(count($post_read_array) > 0){
            return array("data"=>array("status"=>"1", 
                "status_detail"=>"Success", 
                "count"=>count($post_read_array), 
                "result"=>$post_read_array->toArray(), 
                "profile_completion"=>$pass_mark, 
                "matched"=>$matchedJobs->toArray(), 
                "applied"=>$appliedJobs->toArray(), 
                "saved"=>$savedJobs->toArray(), 
                "reffered"=>$refferedJobs->toArray()));
        }else{
            return array("data"=>array("status"=>"0", 
                "status_detail"=>"You currently have no post, this place looks so lonely, Create a post and invite friends to join you here.", 
                "profile_completion"=>$pass_mark));
        }
    }

    public function authenticate($usr, $pass){
        $user = new Application_Model_DbTable_Users();
        $user_search = $user->fetchAll($where="user_email='$usr'");
        //var_dump($user_search[0]->user_pass);
        if(count($user_search) > 0){
            //user possibly exists, compare passwords
            if($user_search[0]->user_pass == $pass){
                $user_details = new Zend_Session_Namespace('user_details');
                $user_details->user_id = $user_search[0]->user_id;
                $user_details->user_name = $user_search[0]->user_name;
                $user_details->user_email = $user_search[0]->user_email;
                $user_details->user_phone = $user_search[0]->user_phone;
                $user_details->user_image = $user_search[0]->user_image;
                $user_details->user_type = $user_search[0]->user_type;
                if($user_search[0]->user_sector > 0){
                    $user_details->user_sector = $user_search[0]->user_sector;
                }else{
                    $user_details->user_sector = 0;
                }
                if(strlen($user_search[0]->user_last_name) < 1){
                    $user_details->user_full_name = $user_search[0]->user_name;
                }else{
                    $user_details->user_full_name =  $user_search[0]->user_first_name . " ".$user_search[0]->user_middle_name . " ". $user_search[0]->user_last_name;
                    
                    $user_details->user_firstname = $user_search[0]->user_first_name;
                    $user_details->user_middlename = $user_search[0]->user_middle_name;
                    $user_details->user_lastname = $user_search[0]->user_last_name;

                }
                
                //user_image
                $auth_token = bin2hex(openssl_random_pseudo_bytes(32));
                //$update_auth_token = "UPDATE users SET auth_token = '$auth_token', user_login_time = NOW() WHERE user_email = '$user' or user_phone = '$user';";
                //$result_array = array("auth_token"=>$auth_token);
                $_SESSION['auth_token'] = $auth_token;
                $_SESSION['token_time'] = time();
                
                setcookie("auth_token", $auth_token, time() + 3600);
                $upd = array("auth_token"=>$auth_token);
                $user->update($upd, "user_email='$usr'");
                return array("data"=>array("status"=>"1", "status_detail"=>"Success", "result"=>$user_details));
            }
        }
        
        /*if($usr == 'harley' && $pass == "Ola"){
            return array("data"=>array("status"=>"1", "status_detail"=>"Success", "result"=>"uwiewoeu"));
        }*/
        return array("data"=>array("status"=>"0", "status_detail"=>"Failed", "result"=>""));
    }
    
    function forgot_password(){
        $user = new Application_Model_DbTable_Users();
        $email = $this->_request->getPost('email');
        $local = str_replace("localhost", "localhost/rebruitonline/public","http://".$_SERVER['HTTP_HOST'] );
            $passw = rand(10000,99999);
            $upd = array("user_pass"=>$passw);
            $update = $user->update($upd, "user_email='$email'");
            $register_temp = file_get_contents($local . "/forgot_template.php");
            $new_mail = str_replace('[username]', "there", $register_temp);
            $new_mail = str_replace('[email]', $email, $new_mail);
            $new_mail = str_replace('[password]', $passw, $new_mail);
            if($update === 1){
                $this->smtpSend("Password reset", $new_mail, array($email));
                $user_details = "";//$passw." is new password, and update value returns ".$update;
                return array("data"=>array("status"=>"1", "status_detail"=>"Password reset successful, check your mailbox($email)."
                    . " <br/> <strong><span onClick='closeModal()' style='cursor:pointer;'>DONE</span></strong>", "result"=>$user_details));
            }else{
                $user_details = "";//$passw." is new password, and update value returns ".$update;
                return array("data"=>array("status"=>"0", "status_detail"=>"Could not reset the password.", "result"=>$user_details));
            }
    }
    
    public function register($email, $mobile, $passw)
    {
        //return array("data"=>array("status"=>"1", "status_detail"=>"Congratulations.", "result"=>""));
        $user = new Application_Model_DbTable_Users();
        $passw = rand(10000,99999);
        $firstname = $this->_request->getPost('firstname');
        $lastname = $this->_request->getPost('lastname');
        $type = $this->_request->getPost('type');
        if (strpos($lastname, '=') !== FALSE || strpos($mobile, '=') !== FALSE || strpos($email, '=') !== FALSE || strpos($firstname, '=') !== FALSE){
            return array("data"=>array("status"=>"0", "status_detail"=>"Please, check your input for wrong values.", "result"=>""));
        }
        $user_ = $user->fetchAll($where="user_email='$email' OR user_name='$email' OR user_phone='$mobile'");
        if(count($user_) > 0){
            //User already exists with this email address
            return array("data"=>array("status"=>"0", "status_detail"=>"This user already exists.", "result"=>""));
        }else{
            //No user with this details, so we can go ahead and register this guy
            $dt = date("Y-m-d H:i:s");
            $data = array("user_first_name"=>$firstname, "user_last_name"=>$lastname, "user_created"=>$dt, "user_type"=>$type, "user_name"=>$email, "user_pass"=>$passw, "user_email"=>$email, "user_phone"=>$mobile, "user_image"=>"assets/img/user.png");
            $id = $user->insert($data);
            $local = str_replace("localhost", "localhost/rebruitonline/public","http://".$_SERVER['HTTP_HOST'] );
            $register_temp = file_get_contents($local . "/template.php");
            $new_mail = str_replace('[username]', $email, $register_temp);
            $new_mail = str_replace('[email]', $email, $new_mail);
            $new_mail = str_replace('[password]', $passw, $new_mail);
            $this->smtpSend("Welcome to Rebruit", $new_mail, array($email));
            return array("data"=>array("status"=>"1", "status_detail"=>"Successfully Registered. Please, check your mailbox for details.", "result"=>$id));
        }
    }

}













