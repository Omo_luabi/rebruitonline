<?php

class RecruiterController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $user_details = new Zend_Session_Namespace('user_details');
        $this->submitAction();
        if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
            $this->userid = $this->view->user_id = $user_details->user_id;
            $this->userid = $user_details->user_id;
            $this->view->user_name = $user_details->user_name;
            $this->view->user_full_name = $user_details->user_full_name;
            $this->view->user_image = $user_details->user_image;
        }else{
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
    }

    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }

    public function indexAction()
    {
        $db_sect = new Zend_Db_Table('rebruit_sectors');
        $this->view->sectors = $db_sect->fetchAll()->toArray();
        // action body
    }


}

