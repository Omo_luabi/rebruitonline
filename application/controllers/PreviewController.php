<?php

class PreviewController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $user_details = new Zend_Session_Namespace('user_details');
        $this->submitAction();
        if(isset($user_details->user_id) && isset($_SESSION['auth_token'])){
            $this->userid = $this->view->user_id = $user_details->user_id;
            $this->view->user_name = $user_details->user_name;
            $this->view->user_full_name = $user_details->user_full_name;
            $this->view->user_image = $user_details->user_image;
        }else{
            $urlOptions = array('controller'=>'Auth', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $userDetails = new Zend_Session_Namespace('userDetails');
            if($userDetails->isLocked()){
                $userDetails->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }

    public function indexAction()
    {
        // action body
        $user = new Application_Model_DbTable_Users();
        $uid = $_GET['profile'];
        if (!(preg_match("/[1-9][0-9]*$/D", $uid))) {
            $uid = $this->view->user_id;
        }
        $user_ = $user->fetchRow($where="user_id='$uid'");
        
        $this->view->this_user_name = $user_->user_name;
        $this->view->this_full_name = $user_->user_last_name . " ". $user_->user_first_name . " ".$user_->user_middle_name;
        
        $this->view->this_dob = $user_->user_dob;
        $this->view->this_gender = $user_->user_gender;
        $this->view->this_user_image = $user_->user_image;
        $this->view->this_marital = $user_->user_marital_status;
        $this->view->this_country = $user_->user_country_id;
        $this->view->this_user_email = $user_->user_email;
        $this->view->this_user_phone = $user_->user_phone;
        $this->view->this_address = $user_->user_address;
        $this->view->this_user_sector = $user_->user_sector;
        $this->view->this_user_employment_status = $user_->user_employment_status;
        $this->view->this_user_job_preference = $user_->user_job_preference;
        
        $db_awards_tb = new Zend_Db_Table('rebruit_awards');
        $this->view->awards = $db_awards_tb->fetchAll($where="rebruit_user_id='$uid'")->toArray();
        
        $db_cert = new Zend_Db_Table('rebruit_certifications');
        $this->view->cert = $db_cert->fetchAll($where="rebruit_user_id='$uid'")->toArray();
        
        $db_edu = new Zend_Db_Table('rebruit_education');
        $this->view->edu = $db_edu->fetchAll($where="rebruit_user_id='$uid'")->toArray();
        
        $db_exp = new Zend_Db_Table('rebruit_experience');
        $this->view->exp = $db_exp->fetchAll($where="rebruit_user_id='$uid'")->toArray();
        
        $db_skills = new Zend_Db_Table('rebruit_skills');
        $this->view->skills = $db_skills->fetchAll($where="rebruit_user_id='$uid'")->toArray();
        
        $db_sect = new Zend_Db_Table('rebruit_sectors');
        $this->view->sectors = $db_sect->fetchAll()->toArray();
    }
    
    public function detailsAction()
    {
        // action body
    }

}

