<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initTimeZone()
    {
            date_default_timezone_set('Africa/Lagos');
    }

    public function _initRoutes(){
        // $front = Zend_Controller_Front::getInstance();
        // $controller = $front->getRequest()->getControllerName();

        // if($controller === 'Api' || $controller === 'api'){
        // 	$router = $front->getRouter();
	       //  $restRoute = new Zend_Rest_Route($front);
	       //  $router->addRoute('default', $restRoute);
        // }
        
        $front = Zend_Controller_Front::getInstance();
        	$router = $front->getRouter();
	        $restRoute = new Zend_Rest_Route($front);
	        $router->addRoute('default', $restRoute);
    }

    protected function _initAutoload()
    {
            // Add autoloader empty namespace
            // $autoloader = Zend_Loader_Autoloader::getInstance();
            // $autoloader->setFallbackAutoloader(true);                                        

            // $admin_loader = new Zend_Application_Module_Autoloader(array(
            //                 'namespace' => 'Admin',
            //                 'basePath' =>APPLICATION_PATH . '/modules/admin'
            // ));                      

            // return $autoloader;     
    }
    protected function _initControllerPlugins()
    {
            // initialize your controller plugins here   
    }

    protected function _initNavigation()
    {
            // initializing navigation       
    }

}

