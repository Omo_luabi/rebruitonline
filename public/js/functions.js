localStorage.education = null;
localStorage.certification = null;
localStorage.experience = null;
localStorage.skills = null;
localStorage.awards = null;

$(document).ready(function(){
    //alert(window.location.href);
    var current_location = document.URL.substr(document.URL.lastIndexOf('/')+1);
    //alert(current_location);
    //$('#searchBox').typeahead();
    $('input.typeahead').typeahead({
          name: 'accounts',
          local: ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen']
        });
    
    var pg = current_location;
    if(pg === '' || pg ==='public' || pg ==='Index' || pg ==='index'){
        pg = 'Index';
        load_posts();
    }
    if(pg === 'Profile'){
        fetchCountry();
    }

    $(".link_"+pg).addClass("active");
    
    document.getElementById('saveImage').onclick = function () {
        document.getElementById("submit-btn").click();
        
        return false; 
    };
    var options = {
        target:   '#output',   // target element(s) to be updated with server response 
        beforeSubmit:  beforeSubmit,  // pre-submit callback 
        success:       afterSuccess,  // post-submit callback 
        resetForm: true        // reset the form after successful submit 
    }; 

    $('#MyUploadForm').submit(function() {
    event.preventDefault();
        $(this).ajaxSubmit(options);  			
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    }); 
});
/***************** IMAGE UPLOAD ******************************/

function getImgFile(){
    imgwaiting(1);
    document.getElementById("imageInput").click();
    //document.getElementById("imageInput").click();
}

function imgwaiting(type){
    if(type === 0){
        document.getElementById('upload_instruction').style.display = "block";
        document.getElementById('upload_loading').style.display = "none";
        return;
    }
    document.getElementById('upload_instruction').style.display = "none";
    document.getElementById('upload_loading').style.display = "block";
}

$("#imageInput").change(function(){
    readURL(this);
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    imgwaiting(0);
}

function afterSuccess(data){
    /*mydiv = document.getElementById("lightbox");
    mydiv.style.display ="none";
    localStorage.setItem("img", data);
    $("#output").html("");
    s2();*/
    //insert this guy's new image id into the database, 
    //then change image in session and display too
    //alert(data);

    if(data.indexOf('error') === -1 || data.indexOf('Warning') === -1){
        id = $("#user_id").val();
        dataString = {
            action:"update_profile_picture",
            user_id:id,
            user_image:data
        };
        makeApiCall(dataString);
        imgwaiting(0);
        show_message("upload_alert", "success", "Successfully Uploaded the new image.");
    }else{
       imgwaiting(0);
        show_message("upload_alert", "danger", "An error occured while uploading this image, please use a different one."); 
    }
    
}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
    imgwaiting(1);
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
            if( !$('#imageInput').val()) //check empty input filed
            {
                show_message("upload_alert", "danger", "Are you kidding me?");
                    //$("#output").html("Are you kidding me?");
                    imgwaiting(0);
                    return false;
            }

            var fsize = $('#imageInput')[0].files[0].size; //get file size
            var ftype = $('#imageInput')[0].files[0].type; // get file type


            //allow only valid image file types 
            switch(ftype)
            {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                            break;
                    default:
                            //$("#output").html("<b>"+ftype+"</b> Unsupported file type!");
                            show_message("upload_alert", "danger", "Unsupported file type!");
                            imgwaiting(0);
                            return false;
            }

            //Allowed file size is less than 1 MB (1048576)
            if(fsize>1048576) 
            {
                    //$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    show_message("upload_alert", "danger", "<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    //show_message("upload_alert", "danger", "Are you kidding me?");
                    imgwaiting(0);
                    return false;
            }
            // mydiv = document.getElementById("lightbox");
            // mydiv.style.display ="block";
            //$('#submit-btn').hide(); //hide submit button
            //$('#loading-img').show(); //hide submit button
            imgwaiting(1);
            //$("#output").html("Please wait!");
            window.location.href = '#';
    }
    else
    {
            //Output error to older unsupported browsers that doesn't support HTML5 File API
            //$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
            show_message("upload_alert", "danger", "Please upgrade your browser, because your current browser lacks some new features we need!");
            imgwaiting(0);
            return false;
    }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
if (bytes == 0) return '0 Bytes';
var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
imgwaiting(0);
return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

/*******************IMAGE UPLOAD ****************************/

function showMsgBox(id, name){
//    html = "<div class='img-rounded' style='border: 1px solid #E6E6E6'> </div>";
//    rec = $('#rec').val();
//    if(rec.trim() != ""){
//        rec = rec+";"+name;
//    }else{
//        rec = name
//    }
//    $('#rec').val(rec);
    loadingHtml = '<div id="loading" style="text-align: center; margin-top:140px;">Loading <i id="spinner" class="icon-spinner icon-spin icon-large"></i></div>';
    
    document.getElementById("read_box").style.display = "block";
    document.getElementById("current_box").style.display = "block";
    $('#read_box').html(loadingHtml);
    document.getElementById("draft_box").style.display = "none";
    showMyMessage(id, name);
}

function checkKey(event){
    //submit_post(\'inbox\')
    if(event.keyCode === 13){
        submit_post('inbox2');       
    }
}

function showMyMessage(id, name){
    $('#current_to_id').val(id);
    $.ajax({
        url: url, 
        type: 'POST',
        dataType: 'json',
        data: {action: "read_message", id:id},
        success:function(data){
            console.log(data.data.result);
            if(data.data.status === '1'){
                html = "";
                for (var i=0; i < data.data.result.length; i++) {
                    
                    msg = data.data.result[i].post_content;
                    if(data.data.result[i].post_user_id === user_id){
                        html += '<div id="msgLine2" class="row"><div id="msg" class="col-xs-10"><div class="triangle-border right">'+msg+'</div></div><div id="img" class="col-xs-1 col-xs-offset-1"><div><img class="msg_img right" src="http://localhost/rebruit/public/assets/img/user.png"/></div></div></div>';
                    }else{
                        html += '<div id="msgLine" class="row"><div id="img" class="col-xs-1"><div ><img class="msg_img" src="http://localhost/rebruit/public/assets/img/user.png"/></div><div id="name" style="overflow:visible; text-align:center: padding-left:10px;">'+name+'</div></div><div id="msg" class="col-xs-10 col-xs-offset-1"><div class="triangle-border left">'+msg+'</div></div></div>';
                    }
                }
                    $('#read_box').html(html);
            }
        }
    });
}

function showDraft(){
    document.getElementById("read_box").style.display = "none";
    document.getElementById("draft_box").style.display = "block";
    document.getElementById("current_box").style.display = "none";
}

function index_side_bar(array_response){
    completed = array_response.profile_completion;
    complete_percentage = (parseInt(completed)/6)*100;
    //alert(complete_percentage);
    if(complete_percentage <= 50){
        $('#profile_completion').addClass('progress-bar-danger');
        document.getElementById('profile_completion_div').style.display = 'block';
    }else if(complete_percentage > 50 && complete_percentage < 100){
        $('#profile_completion').addClass('progress-bar-warning');
        document.getElementById('profile_completion_div').style.display = 'block';
    }else{
        document.getElementById('profile_completion_div').style.display = 'none';
    }
    $('#profile_completion').width(complete_percentage+"%"); 
    
    wd = $('div.progress').css("width");
    wd = wd.substring(0, (wd.length)-2);
    wd = parseInt(wd)/2;
    pcenthtml = '<div id="pcent" style="left:'+wd+'px; color:black; position:absolute;">'+Math.round(complete_percentage)+' %</div>';
    $('#profile_completion').append(pcenthtml);
    
    var matched = '';
    var applied = '';
    var saved = '';
    var reffered = '';
    
    for (var i=0; i < array_response.matched.length; i++) {
        matched += '<a style="cursor:pointer;" data-toggle="modal" data-target="#other_info" onClick="post_operations('+array_response.matched[i].job_post_id+',\'job_detail\')" class="list-group-item">'+array_response.matched[i].job_post_title+'</a>';
    }
    for (var i=0; i < array_response.applied.length; i++) {
        applied += '<a style="cursor:pointer;" data-toggle="modal" data-target="#other_info" onClick="post_operations('+array_response.applied[i].job_post_id+',\'job_detail\')" class="list-group-item">'+array_response.applied[i].job_post_title+'</a>';
    }
    for (var i=0; i < array_response.saved.length; i++) {
        saved += '<a style="cursor:pointer;" data-toggle="modal" data-target="#other_info" onClick="post_operations('+array_response.saved[i].job_post_id+',\'job_detail\')" class="list-group-item">'+array_response.saved[i].job_post_title+'</a>';
    }
    for (var i=0; i < array_response.reffered.length; i++) {
        reffered += '<a style="cursor:pointer;" data-toggle="modal" data-target="#other_info" onClick="post_operations('+array_response.reffered[i].job_post_id+',\'job_detail\')" class="list-group-item">'+array_response.reffered[i].job_post_title+'</a>';
    }
    $('#matched_jobs').html(matched);
    $('#applied_jobs').html(applied);
    $('#saved_jobs').html(saved);
    $('#referred_jobs').html(reffered);
}

function show_message(div, type, msg){
    if(type === "reset"){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).removeClass("alert-success");
        document.getElementById(div).style.display = "none";
        return;
    }
    if(type === 'success'){
        $("#"+div).removeClass("alert-danger");
    }else { $("#"+div).removeClass("alert-success"); }
        $("#"+div).addClass("alert-"+type);
        $("#"+div).text(msg);
        document.getElementById(div).style.display = "block";
}
    
function add_row(where){
    //edu, cert, exp, skill, award
    switch (where){
        case 'edu':
            add_row_edu('education');
        break;
        case 'cert':
            add_row_cert('certification');
        break;
        case 'exp':
            add_row_exp('experience');
        break;
        case 'skill':
            add_row_skill('skills');
        break;
        case 'award':
            add_row_award('awards');
        break;
    }
}

function apply_save_job(id, type){
    dataString = {
        action : 'save_apply_jobs',
        job_post_id : id,
        action_type : type
    };
    dataString.div = "main_alert";
    makeApiCall(dataString);
}

function save_other_profile(id, profile_div_name){
    show_message(profile_div_name+"_alert", "reset");
    var values = {};
    var fields = $('#'+profile_div_name+' :input');
    $.each(fields, function(i, field) {
        var dom = $(field),
            name = dom.attr('id'),
            value = dom.val();
        values[name] = value;
    });
    console.log(values);
    dataString = values;
    dataString.div = profile_div_name;
    dataString.action = "update_other_profile";
    dataString.user_id = id;
    switch (profile_div_name){
        case 'education_profile':
            dataString.array = localStorage.education;
        break;
        case 'certification_profile':
            dataString.array = localStorage.certification;
        break;
        case 'experience_profile':
            dataString.array = localStorage.experience;
        break;
        case 'skill_profile':
            dataString.array = localStorage.skills;
        break;
        case 'award_profile':
            dataString.array = localStorage.awards;
        break;
    }
    makeApiCall(dataString);
}
        
function save_personal_profile(id){
    //document.forms["personal_profile"].submit();
    show_message("personal_profile_alert", "reset");
    var values = {};
    var fields = $('#personal_profile :input');
    $.each(fields, function(i, field) {
        var dom = $(field),
            name = dom.attr('id'),
            value = dom.val();
        values[name] = value;
    });
    console.log(values);

    //return;
    
    /*full_name = $("#p_full_name").val();
    dob = $("#p_dob").val();
    gender = $("#p_gender").val();
    marital = $("#p_marital").val();
    country = $("#p_nationality").val();
    address = $("#p_address").val();
    if(full_name.indexOf(' ') === -1 || dob === '' || gender === '' || marital === '' || country === '' || address === ''){
        show_message("personal_profile_alert", "danger", "Please ensure you have entered all fields correctly!");
        return;
    } */
    dataString = values;
    dataString.action = "update_personal_profile";
    dataString.user_id = id;
     // dataString = {
     //            action:"update_personal_profile",
     //            user_id:id,
     //            full_name:full_name,
     //            dob:dob,
     //            gender:gender,
     //            marital:marital,
     //            country:country,
     //            address:address
     //        };
    makeApiCall(dataString);
}

function submit_post(type){
    
    msg = $("#post_form").val();
    to_id = $("#post_to_id").val();
    if(type === "inbox2"){
        msg = $("#current_msg").val();
        to_id = $("#current_to_id").val();
    }
    id = user_id;
    name = user_name;
    if(msg === ""){
        return;
    }
    post_array = {
        action:"submit_post",
        post_id:0,
        post_user_id:id,
        user_name:name,
        post_type:type,
        post_content:msg,
        post_to_id:to_id,
        post_name:name,
        post_time:"now",
        user_image:my_img
    };
    //insert_post();
    makeApiCall(post_array);
    $("#post_form").val('');
}

function submit_message(){
    
}

function switch_to_job(){
    document.getElementById("job_details").style.display = "block";
    document.getElementById("job_button").style.display = "none";
    document.getElementById("post_job_button").style.display = "block";
}

function submit_job_post(){
//    $('#desc').val(CKEDITOR.instances.desc_ck.getData());
    title = $("#title").val();
    company = $("#company").val();
    loc = $("#pac-input").val();
    desc = CKEDITOR.instances.desc_ck.getData();//$("#desc").val();
    end_date = $("#date").val();
    sector = $("#sector").val();
    salary = $("#salary").val();
    id = $("#user_id").val();
    name = $("#user_name").val();
    if(title === "" || desc === ""){
        return;
    }
    post_array = {
                action:"submit_job_post",
                company:company,
                location:loc,
                post_id:0,
                post_user_id:id,
                user_name:name,
                post_type:"job",
                post_content:title,
                post_desc:desc,
                post_end_date:end_date,
                post_sector:sector,
                post_salary:salary,
                post_to_id:2,
                post_name:name,
                post_time:"now",
                user_image:my_img
            };
    //insert_post();
    makeApiCall(post_array);
    $("#post_form").val('');
}

function load_posts(){
    id = $("#user_id").val();
    dataString = {
                action:"read_post",
                user_id:id
            };
    makeApiCall(dataString);
}

function show_posts(post_array, who){
    try{
        lk = (((",", post_array['post_like']+"padding").match(/,/g) || []).length) - 1;
        br = (((",", post_array['post_bruit']+"padding").match(/,/g) || []).length) - 1;
        co = (((",", post_array['post_comment']+"padding").match(/,/g) || []).length) - 1;
    }catch(Exception){
        lk = '';
        br = '';
        co = '';
    }
    
    like_but = "Like";
    usr = new RegExp(user_id, 'i');
    if((post_array['post_like']).match(usr)){
        like_but = "Liked";
    }
    
    sendr_name = post_array['user_first_name'] + " " + post_array['user_last_name'];
    post_id = post_array['post_id'];
    pr = "op_"+post_id+"_";
    close = '';
    jobOperations ='';
    otherDetails = '';
    if(who === "me" || user_id === post_array['post_user_id']){
        close = '<span aria-hidden="true" style="float:right;" onClick=\'post_del('+post_id+',"delete")\'><i class="icon-trash"></i></span>';//&times;
    }//.op_6_like, .op_6_bruit, op_6_comment
    if(post_array['post_type'] === 'job'){//job_post_description, job_post_due, job_post_salary, job_vacancy_
        coy = "";
        if(post_array['job_vacancy_company'] ===  ""){
            coy = sendr_name;
        }else{
            coy = post_array['job_vacancy_company'];
        }
        //post_array['job_post_description']
        op = "<div id='op' class='options'> <span class='icon-thumbs-up-alt option_item' onClick='post_operations("+
                post_id+",\"like\")'> "+like_but+" <span class='"+pr+"like'>"+lk+"</span></span> <span class='icon-bullhorn option_item' onClick='post_operations("+
                post_id+",\"bruit\")'> Bruit <span class='"+pr+"bruit'>"+br+"</span></span> <!--<span class='icon-comment-alt option_item' onClick='post_comment("+
                post_id+",\"comment\")'> Comment <span class='"+pr+"comment'>"+co+"</span></span> --> </div>";//onClick="apply_save_job('.$this->all[$i]['job_post_id'].',\'apply\')"
        otherDetails = "<div style='margin-top:5px; margin-bottom:5px; font-size:smaller;'><div class='row'><span class='col-xs-10'>"+
                post_array['post_content']+"</span></div><div class='row'><span class='col-xs-10'>"+
                post_array['job_vacancy_location']+"</span></div></div>";
        jobOperations = "<div id='op' style='float:none;' class='options'><span data-toggle='modal' data-target='#other_info' class='fa fa-list option_item' onClick='post_operations("+
                post_array['post_reference']+",\"job_detail\")'> Details </span> <span class='fa fa-floppy-o option_item' onClick='apply_save_job("+
                post_array['post_reference']+",\"save\")'> Save </span> <span class='icon-thumb_tack option_item' onClick='apply_save_job("+
                post_array['post_reference']+",\"apply\")'> Apply </span></div>";
        html = "<div id='post' class='post_"+post_id+"'><div class='post'>"+
            "<span class='post-name'><img src='"+post_array['user_image']+"' style='' class='img-thumbnail img-post' alt='Profile Image'> <a href='Preview?profile="+post_array['post_user_id']+"'>"+sendr_name+"</a></span>"+close+"<hr style='margin-top:10px;'/>"+
            "<div id='says'><div class='col-xs-3' style='text-align:center; margin-top:20px;'><span style='font-size:xx-large' class='icon-suitcase'></span></div>"+
            "<div  class='col-xs-9'><div style='font-size:x-large'>"+coy+"</div>"+
            "<div>"+otherDetails + jobOperations+"</div></div></div><br/>"+
            "<div id='time' class='time'>"+timeSince(post_array['post_time'])+"</div>"+ op +
            "</div></div>";
    }else{
        op = "<div id='op' class='options'> <span class='icon-thumbs-up-alt option_item' onClick='post_operations("+post_id+",\"like\")'> "+like_but+" <span class='"+pr+"like'>"+lk+"</span></span> <span class='icon-bullhorn option_item' onClick='post_operations("
                +post_id+",\"bruit\")'> Bruit <span class='"+pr+"bruit'>"+br+"</span></span> <!-- <span class='icon-comment-alt option_item' onClick='post_comment("
                +post_id+",\"comment\")'> Comment <span class='"+pr+"comment'>"+co+"</span></span> --> </div>"+
"<div style='clear:both;'></div>";
        html = "<div id='post' class='post_"+post_id+"'><div class='post'>"+
            "<span class='post-name'><img src='"+post_array['user_image']+"' style='' class='img-thumbnail img-post' alt='Profile Image'> <a href='Preview?profile="+post_array['post_user_id']+"'>"+sendr_name+"</a></span>"+close+"<hr style='margin-top:10px;'/>"+
            "<div id='says'><div>"+post_array['post_content']+"</div>"+
            "<div>"+otherDetails + jobOperations+"</div></div><br/>"+
            "<div id='time' class='time'>"+timeSince(post_array['post_time'])+"</div>"+ op +
            "</div></div>";
    }
    
    
    if(who === 'me'){
        $('#posts').prepend(html);
    }else{
        $('#posts').append(html);
    }  
}

function timeSince(date) {
    var seconds = Math.floor((new Date() - new Date(date)) / 1000);
    
    var interval = Math.floor(seconds / 31536000);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " year ago";
        }
        return "about " + interval + " years ago";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " month ago";
        }
        return "about " + interval + " months ago";
    }
    interval = Math.floor(seconds / 604800);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " week ago";
        }
        return "about " + interval + " weeks ago";
    }
    interval = Math.floor(seconds / 86400);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " day ago";
        }
        return "about " + interval + " days ago";
    }
    interval = Math.floor(seconds / 3600);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " hour ago";
        }
        return "about " + interval + " hours ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval >= 1) {
        if(interval === 1){
            return "about " + interval + " minute ago";
        }
        return "about " + interval + " minutes ago";
    }
    return Math.floor(seconds) + " seconds";
}


//my_img
function post_comment(id, type){
    text_image = "<span class=''><img style='vertical-align:top;' src='"+my_img+"' style='' class='img-thumbnail img-post' alt='Profile Image'><span id='txt'><textarea style='height:40px; width:calc(100% - 40px)'></textarea></span></span>";
    text_image += "<div style='text-align:right;'><button type='submit' id='btn_"+id+"' class='btn btn-success' onClick='post_operations("+id+",\"comment\")'>Comment</button></div>";
    
    html = "<hr/> <div style='height:100px; background-color:white;'>" +text_image+ "</div>"
    $('.post_'+id+' .post').append(html);
}

function post_del(post_id, type){
    alertify.confirm('Closable: false').setting('closable', false); 
    alertify.dialog('confirm')
      .setting({
            'title':'Delete Post',
            'labels':{ok:'Delete!', cancel:'Cancel'},
            'message': "Do you really want to delete this post?.",
            'onok': function(){ 
                    post_operations(post_id, type);
                    alertify.success("Post Deleted!");
            },
            'oncancel': function(){ }
    }).show();
}

function post_operations(post_id,type){
    //like_bruit_comment
    switch (type){
        case "job_detail" :
            $('#other_info_content').html("<div style='text-align:center;'> Please wait <i id='spinner' class='icon-spinner icon-spin icon-large'></i> </div>");
            dataString = {
                    action:type,
                    user_id:id,
                    post_id:post_id,
                    type:type
                };
            makeApiCall(dataString);
            break;
        case "comment" :
            dataString = {
                action:"submit_comment",
                userid:id,
                post_id:post_id,
                type:type,
                comment:$('.post_'+post_id+' .post div span textarea').val()
            };
            makeApiCall(dataString);
            break;
        default :
            dataString = {
                action:"like_bruit_comment",
                user_id:id,
                post_id:post_id,
                type:type
            };
            makeApiCall(dataString);
            break;
    }
    
}

function set_alert_timed(div, type, msg){
    show_message(div,type, msg);
    setTimeout(function () {show_message(div, "reset")}, 5000); //5 seconds
    //show_message(div, "reset")
}

function makeApiCall(dataString){
//    console.log(dataString);
//    return;
    $.ajax({
       type: 'POST',
            url: url,
            data: dataString,
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            success: function(json) {
            //console.log(json);
               if(json.data.status === '1'){
           //alert('success');
                if(dataString.action === "submit_post"){
                    if(dataString.post_type === "inbox"){
                        set_alert_timed("post_alert", "success", "Message has been delivered");
                        return;
                    }else if(dataString.post_type === "inbox2"){
                        html = '<div id="msgLine2" class="row"><div id="msg" class="col-xs-10"><div class="triangle-border right">'+dataString.post_content+'</div></div><div id="img" class="col-xs-1 col-xs-offset-1"><div><img class="msg_img right" src="http://localhost/rebruit/public/assets/img/user.png"/></div></div></div>';
                            $('#read_box').prepend(html);
                            return;
                    }
                    set_alert_timed("post_alert", "success", "Nice! You just updated your timeline");
                    dataString.post_id = json.data.result;
                    dataString.post_time = json.data.time;
                    show_posts(dataString, 'me');
                }else if(dataString.action === "submit_job_post"){
                    set_alert_timed("post_alert", "success", "Job has been posted");
                }else if(dataString.action === "read_post"){                       
                     //alert(json.data.profile_completion);
                     index_side_bar(json.data);
                     for (var i=0; i < json.data.count; i++) {
                         post_array = json.data.result[i];
                         show_posts(post_array);
                     }
                }else if(dataString.action === "update_personal_profile"){
                    show_message("personal_profile_alert", "success", "Personal profile update successful!");
                 }else if(dataString.action === 'update_profile_picture'){
                     newimg = base + dataString.user_image;
                     document.getElementById("closeupload").click();
                     $('#profileimg').attr('src', dataString.user_image);
                 }else if(dataString.action === "update_other_profile"){
                     //profile_div_name
                     show_message(dataString.div+"_alert", "success", "Profile update successful!");
                 }else if(dataString.action === "save_apply_jobs"){
                     show_message("all_jobs_alert", "success", json.data.status_detail);
                 }else if(dataString.action === "fetch_country"){
                       showCountry(json.data.result);
                       //console.log(json.data.result[0][1]);
                }else if(dataString.action === "like_bruit_comment"){
                    if(dataString.type === "delete"){
                        $(".post_"+ dataString.post_id).fadeOut(); 
                    }else{
                        //.op_6_like, .op_6_bruit, op_6_comment
                        c = parseInt($(".op_"+ dataString.post_id+"_"+dataString.type).text());
                        $(".op_"+ dataString.post_id+"_"+dataString.type).text(c+1);
                    }
                }else if(dataString.action === "job_detail"){
                    $('#other_info_content').html(json.data.result);
                }else if(dataString.action === "submit_comment"){
                    message = $('.post_'+dataString.post_id+' .post div span span#txt textarea').val();
                    $('.post_'+dataString.post_id+' .post div span span#txt').html('<span style="padding:5px;">' + message + '</span>');
                    $('.post_'+dataString.post_id+' .post div div btn_'+dataString.post_id).css("display", "none");
                }//End of condition 1
               }else{
                if(dataString.action === "submit_post" || dataString.action === "submit_job_post"){
                     set_alert_timed("post_alert", "danger", json.data.status_details);
                 }else if(dataString.action === 'update_profile_picture'){
                     show_message("upload_alert", "danger", json.data.status_details);
                 }
               }
            },
            error: function(e) {
               console.log(e);
               pleaseWait(0);
            },
            complete: function(){
                    pleaseWait(0);
            }
    });
       
}

function showCountry(result){
    for(i = 0; i < result.length; i++){
        selected = ""
        if(result[i]["id"] === $('#country_selected').val()){
            selected = "selected";
        }
        html = "<option value='"+result[i]["id"]+"' "+selected+">"+result[i]["country"]+"</value>";
        $('#p_nationality').append(html);
    }
}

function remove_row(what, name, id){
    //alert("remove index from "+what);
    curr_val = localStorage.getItem(name);
    curr_val_arr = curr_val.split(',');
    ind = curr_val_arr.indexOf(id);
    //alert("Index of what to remove is "+ind);
    
    console.log('ID is '+id+' curr_val_array is '+curr_val_arr);
    curr_val_arr.splice(ind,1);
    localStorage.setItem(name, curr_val_arr);
    console.log('New array is '+curr_val_arr+' old is '+localStorage.getItem(name));
    document.getElementById(what).style.display = "none";
}

/*
education = {};
        certification = {};
        experience = {};
        skills = {};
        awards = {};
*/

function add_row_edu(name){
    //add id to each row so i can delete and submit each of them
    if(localStorage.getItem(name) === null || localStorage.getItem(name) === 'null'){ 
        localStorage.setItem(name, "1"); 
        ind = "1";
    }else{ 
        curr = localStorage.getItem(name);
        console.log("storage is "+curr);
        curr_arr = curr.split(',');
        console.log(curr_arr[curr_arr.length-1]);
        ind = parseInt(curr_arr[curr_arr.length-1]) + 1;
        //localStorage.education = curr + ","+ind;
        localStorage.setItem(name, curr + ","+ind); 
    }
    var row = '<div id="edu'+ind+'"><div class="row">'+
    '<div class="col-sm-4"><input id="edu_institution_'+ind+'" class="post_form" placeholder="School"/></div>'+
    '<div class="col-sm-3"><input id="edu_qualification_'+ind+'" class="post_form" placeholder="Course of Study"/></div>'+
    '<div class="col-sm-2"><input id="edu_yearout_'+ind+'" class="post_form" placeholder="Year Graduated"/></div>'+
    '<div class="col-sm-2"><input id="edu_grade_'+ind+'" class="post_form" placeholder="Degree Level"/></div>'+
    '<div class="col-sm-1"><i title="Remove row" onClick="remove_row(\'edu'+ind+'\', \''+name+'\',\''+ind+'\')" class="icon-minus"></i></div></div><hr/></div>';
    $('#edu_rows').prepend(row);
}

function add_row_cert(name){
    //add id to each row so i can delete and submit each of them
    if(localStorage.getItem(name) === null || localStorage.getItem(name) === 'null'){ 
        localStorage.setItem(name, "1"); 
        ind = "1";
    }else{ 
        curr = localStorage.getItem(name);
        console.log("storage is "+curr);
        curr_arr = curr.split(',');
        console.log(curr_arr[curr_arr.length-1]);
        ind = parseInt(curr_arr[curr_arr.length-1]) + 1;
        localStorage.setItem(name, curr + ","+ind); 
    }
    var row = '<div id="cert'+ind+'"><div class="row">'+
            '<div class="col-sm-4"><input id="cert_provider_'+ind+'" class="post_form" placeholder="Provider"/></div>'+
            '<div class="col-sm-4"><input id="cert_certification_'+ind+'" class="post_form" placeholder="Certification"/></div>'+
            '<div class="col-sm-3"><input id="cert_year_'+ind+'" class="post_form" placeholder="Year"/></div>'+
            '<div class="col-sm-1"><i title="Remove row" onClick="remove_row(\'cert'+ind+'\', \''+name+'\',\''+ind+'\')" class="icon-minus"></i></div></div><hr/></div>';
    
    $('#cert_rows').prepend(row);
}

function add_row_exp(name){
    //add id to each row so i can delete and submit each of them
    if(localStorage.getItem(name) === null || localStorage.getItem(name) === 'null'){ 
        localStorage.setItem(name, "1"); 
        ind = "1";
    }else{ 
        curr = localStorage.getItem(name);
        console.log("storage is "+curr);
        curr_arr = curr.split(',');
        console.log(curr_arr[curr_arr.length-1]);
        ind = parseInt(curr_arr[curr_arr.length-1]) + 1;
        localStorage.setItem(name, curr + ","+ind); 
    }
    var row = '<div id="exp'+ind+'"><div class="row">'+
            '<div class="col-sm-3"><input id="exp_company_'+ind+'" class="post_form" placeholder="Employer"/></div>'+
            '<div class="col-sm-2"><input id="exp_title_'+ind+'" class="post_form" placeholder="Description"/></div>'+
            '<div class="col-sm-2"><input id="exp_from_'+ind+'" class="post_form" placeholder="Start Date"/></div>'+
            '<div class="col-sm-2"><input id="exp_to_'+ind+'" class="post_form" placeholder="End Date"/></div>'+
            '<div class="col-sm-2"><input id="exp_desc_'+ind+'" class="post_form" placeholder="Description"/></div>'+
            '<div class="col-sm-1"><i title="Remove row" onClick="remove_row(\'exp'+ind+'\', \''+name+'\',\''+ind+'\')" class="icon-minus"></i></div></div><hr/></div>';
    
    $('#exp_rows').prepend(row);
}

function add_row_skill(name){
    //add id to each row so i can delete and submit each of them
    if(localStorage.getItem(name) === null || localStorage.getItem(name) === 'null'){ 
        localStorage.setItem(name, "1"); 
        ind = "1";
    }else{ 
        curr = localStorage.getItem(name);
        console.log("storage is "+curr);
        curr_arr = curr.split(',');
        console.log(curr_arr[curr_arr.length-1]);
        ind = parseInt(curr_arr[curr_arr.length-1]) + 1;
        localStorage.setItem(name, curr + ","+ind); 
    }
    var row = '<div id="skill'+ind+'"><div class="row">'+
            '<div class="col-sm-11"><input id="skill_detail_'+ind+'" class="post_form" placeholder="Skill Type"/></div>'+
            '<div class="col-sm-1"><i title="Remove row" onClick="remove_row(\'skill'+ind+'\', \''+name+'\',\''+ind+'\')" class="icon-minus"></i></div></div><hr/></div>';
    
    $('#skill_rows').prepend(row);
}

function add_row_award(name){
    //add id to each row so i can delete and submit each of them
    if(localStorage.getItem(name) === null || localStorage.getItem(name) === 'null'){ 
        localStorage.setItem(name, "1"); 
        ind = "1";
    }else{ 
        curr = localStorage.getItem(name);
        console.log("storage is "+curr);
        curr_arr = curr.split(',');
        console.log(curr_arr[curr_arr.length-1]);
        ind = parseInt(curr_arr[curr_arr.length-1]) + 1;
        localStorage.setItem(name, curr + ","+ind); 
    }
    var row = '<div id="skill'+ind+'"><div class="row">'+
            '<div class="col-sm-4"><input id="award_type_'+ind+'" class="post_form" placeholder="Skill Type"/></div>'+
            '<div class="col-sm-5"><input id="award_body_'+ind+'" class="post_form" placeholder="Detail"/></div>'+
            '<div class="col-sm-2"><input id="award_year_'+ind+'" class="post_form" placeholder="Year"/></div>'+
            '<div class="col-sm-1"><i title="Remove row" onClick="remove_row(\'skill'+ind+'\', \''+name+'\',\''+ind+'\')" class="icon-minus"></i></div></div><hr/></div>';
    
    $('#award_rows').prepend(row);
}

function fetchCountry(){
    dataString = {
        action:"fetch_country"
    };
    makeApiCall(dataString);
}

function pleaseWait(){
    
}

function searchsuggest(){
    alert("");
    return ["Alli Abdulateef", "Olamide", "Omo_luabi"];
}
//update users set user_image = "assets/img/"+user_image where user_image NOT LIKE "assets/img/%"
// autocomplet : this function will be executed every time we change the text
function autocomplet() {
    var min_length = 2; // min caracters to display the autocomplete
    var keyword = $('#searchBox').val();
    if (keyword.length >= min_length) {
        $.ajax({
            url: url,
            type: 'POST',
            data: {action: "search_string", keyword:keyword},
            success:function(data){
                $('#search_list_id').show();
                $('#search_list_id').html(data);
            }
        });
    } else {
        $('#search_list_id').hide();
    }
}
function set_item(item) {
    // change input value
    $('#country_id').val(item);
    // hide proposition list
    $('#country_list_id').hide();
}

function set_item(item,type) {
    // change input value
    $('#country_id').val(item);
    // hide proposition list
    $('#country_list_id').hide();
}

function autocomplet2() {
    var min_length = 2; // min caracters to display the autocomplete
    var keyword = $('#post_to_name').val();
    if (keyword.length >= min_length) {
        $.ajax({
            url: url, 
            type: 'POST',
            data: {action: "search_string", keyword:keyword, type:"message"},
            success:function(data){
                $('#search_list_id2').show();
                $('#search_list_id2').html(data);
            }
        });
    } else {
        $('#search_list_id2').hide();
    }
}

function select_item(id, name){
    window.location.href = "Preview?profile="+id;
}

function select_item2(id, name){
    $('#post_to_name').val(name);
    $('#post_to_id').val(id);
    $('#search_list_id2').hide();
}

function job_apply(){
    
}
 
