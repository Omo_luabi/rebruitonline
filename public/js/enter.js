function fetchCountry(){
    dataString = {
        action:"fetch_country"
    };
    makeApiCall(dataString);
}

$("#creg").change(function(){
    $("#countryCode").text($("#creg").val());
});

//$("#sreg").change(function(){
//    if($('#sreg').val() === 0 || $('#sreg').val() === "0"){
//       
//    }else{
//        rEg();
//    }
//});

function res(){
    //$("#sreg").val(0);
}

function lIn(){
    document.getElementById('bullhorn').style.display = 'none';
    document.getElementById('spinner').style.display = 'block';
    var m = $('#sIn div #email').val();
    var p = $('#sIn div #password').val();
    if(m === '' || p === ''){
        loginFailed();
        return;
    }
    dataString = {
                    action:'auth',
                    usr:m,
                    pw:p
            };
    makeApiCall(dataString);
    //window.location.href = "Index";
}

function fGet(){
    dataString = {
                    action:'forgot_password',
                    email:$('#fget_email').val()
            };
    makeApiCall(dataString);
    //forgot_alert
    //document.getElementById("closemodal").click();
}

function closeModal(){
    document.getElementById("closemodal").click();
}

function rEg(){
    if($('#sreg').val() === 0 || $('#sreg').val() === "0"){
        return;
    }
    var f = $('#freg').val();
    var l = $('#lreg').val();
    var e = $('#ereg').val();
    var m = $("#countryCode").val() + $('#mreg').val();
    var p = '';
    var t = "";
    if (document.getElementById('seeker').checked) {
          t = "3";
      }else{
          t = "2";
      }
    var ind1 = e.indexOf('@');
    var ind2 = e.indexOf('.');
    if(ind2 != -1 && ind1 != -1 && ind2 >ind1){
        if(f != "" && e != "" && m != "" && l != ""){
            document.getElementById('reg-bullhorn').style.display = 'none';
            document.getElementById('reg-spinner').style.display = 'block';
    //        alert("Confirm");
            dataString = {
                        action:'register',
                        email:e,
                        mobile:m,
                        pw:p,
                        type:t,
                        firstname:f,
                        lastname:l
                };
            makeApiCall(dataString);
        }else{
            show_message("reg_alert", "danger", "Please, fill all fields to continue.");
        }
        
    }else{
        //Throw validation errpr here email_reg_item
        $('#email_reg_item').addClass("has-error");
        document.getElementById('eregStatus').style.display = 'block';
        return;
    }
}

function pullRandomJobs(){
	
}

function pullPatners(){
	
}

function pullUsers(){
	
}

function pleaseWait(type){//0 is off, 1 is on
    
}

function checkButton(event){
    if(event.keyCode === 13){
        lIn();        
    }	
}

function show_message(div, type, msg){
    if(type === "reset"){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).removeClass("alert-success");
        document.getElementById(div).style.display = "none";
        return;
    }
    if(type === 'success'){
        $("#"+div).removeClass("alert-danger");
    }else { $("#"+div).removeClass("alert-success"); }
        $("#"+div).addClass("alert-"+type);
        $("#"+div).html(msg);
        document.getElementById(div).style.display = "block";
}

function makeApiCall(dataString){
    
	$.ajax({
	   type: 'POST',
		url: url,
		data: dataString,
		async: true,
		jsonpCallback: 'jsonCallback',
		dataType: 'json',
		success: function(json) {
                    console.log(json);
		   if(json.data.status === '1'){
                       //alert('success');
                        if(dataString.action === "auth"){
                          window.location.href = "Index";
                        }else if(dataString.action === "register"){
                           //Todo after Registeraion
                            document.getElementById('reg-bullhorn').style.display = 'block';
                            document.getElementById('reg-spinner').style.display = 'none';
                           show_message("reg_alert", "success", json.data.status_detail);
                        }else if(dataString.action === 'forgot_password'){
                           show_message("forgot_alert", "success", json.data.status_detail);
                        }else if(dataString.action = "fetch_country"){
                           showCountry(json.data.result);
                           //console.log(json.data.result[0][1]);
                        }else{
                           
                        }
                       console.log(json);
		   }else{
                       if(dataString.action === "auth"){
                            loginFailed();
                       }else if(dataString.action === "register"){
                            document.getElementById('reg-bullhorn').style.display = 'block';
                            document.getElementById('reg-spinner').style.display = 'none';
                           show_message("reg_alert", "danger", json.data.status_detail);
                       }else if(dataString.action === 'forgot_password'){
                           show_message("forgot_alert", "danger", json.data.status_detail);
                       }
		   }
		},
		error: function(e) {
                   console.log(e);
		   pleaseWait(0);
		},
		complete: function(){
			pleaseWait(0);
		}
	});
}

function showCountry(result){
    html = ""
    for(i = 0; i < result.length; i++){
        html = "<option value='"+result[i]["code"]+"'>"+result[i]["country"]+"</value>";
//        document.getElementById("country").options.add(html);
//        var x = document.getElementById("country");
        $('#creg').append(html);
//        var option = document.createElement("option");
//        option.text = result[i]["country"];
//        option.nodeValue = result[i]["code"];
//        x.add(option);
    }
}

function loginFailed(){
    $('#email_login_item').addClass("has-error");
    $('#pw_login_item').addClass("has-error");
    
    document.getElementById('emailStatus').style.display = 'block';
    document.getElementById('passwordStatus').style.display = 'block';
    document.getElementById('bullhorn').style.display = 'block';
    document.getElementById('spinner').style.display = 'none';
}
