<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Olamide Alli omo_luabi">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>Rebruit</title>

    <!-- Bootstrap core CSS -->
    <?php $local = str_replace("localhost", "localhost/rebruitonline/public","http://".$_SERVER['HTTP_HOST'] );?>
    <link href="<?php echo $local; ?>/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $local; ?>/dashboard.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $local; ?>/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<style>
body{
 font-family:comfortaa;
 background-color:#EEE;
}
.sign_up_input{
	font-size:larger;
}
</style>
  <body style='background-image:url("<?php echo $local; ?>/assets/img/img2bg.jpg"); background-size:100%;'>

<!--MODAL VIEW ENDS HERE-->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style='font-family:comfortaa;'>
			  <img src='<?php echo $local; ?>/assets/img/header.png' style='height:35px; margin-top:-10px;'/>
			  Rebruit
		  </a>
        </div>
        <div class="navbar-collapse collapse">
		  
        </div><!--/.navbar-collapse -->
      </div>
    </div>
	
<div class="container">
      <div class="row" style='padding:50px; background-image:url("<?php echo $local; ?>/assets/fancybox_overlay.png"); background-size:100%;'>
          <div class="col-sm-12">
            <div class='row' style='padding:25px; font-size:larger; color:black; overflow-y:hidden; background-color: whitesmoke;'>
                Hi <b>[username]</b>,
                <br/><br/>
                We see you have forgotten your password.
                <br/>
                <br/>
                Please find within this mail, a newly generated randomized password.
                <br/>
                <br/>
                It is highly advisable that you change this password to something more secured and convenient for you.
                <br/>
                <br/>
                Please, let's keep the community safe and sane.
                <br/>
                <br/>
                <div>
                    Username : [email]
                    <br/>
                    Password : [password]
                </div>
                </br>
                Regards,<p>
                    Rebruit team.
                </p>
            </div>
          </div>
      </div>
      <p><!--<a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a>--></p>
    </div>
  </body>
</html>

